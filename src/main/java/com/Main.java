package com;

import java.util.*;

import org.apache.logging.log4j.*;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);
    static List<Integer> list = new ArrayList<>();

    public static void main(String[] args) {
        firstTaskStart();
        thirdTaskStart();
    }

    static public void firstTaskStart() {
        FunctionalInterface maxValue = (a, b, c) -> Arrays.asList(a, b, c).stream().max(Integer::compare).get();
        FunctionalInterface avgValue = (a, b, c) -> (a + b + c) / 3;
        logger.info("max value =  " + maxValue.accept(20, 30, 44));
        logger.info("average value =  " + avgValue.accept(22, 55, 18));
    }

    static public void thirdTaskStart() {
        makeRandomList();
        IntSummaryStatistics statistics = list.stream().mapToInt(i -> i).summaryStatistics();
        OptionalDouble averageInterface = list.stream().mapToDouble(Double::valueOf).average();
        double average = averageInterface.orElse(0);
        double biggerAverage = list.stream().filter(d -> d > average).count();

        logger.info(statistics);
        logger.info(list);
        logger.info("Bigger than average =  " + biggerAverage);
    }

    static public void makeRandomList() {
        Random random = new Random();
        int rand = 0;
        for (int i = 0; i < 10; i++) {
            rand = random.nextInt(30);
            list.add(i, rand);
        }
    }
}
